<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class ItemForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('errors', 'static', [
                'tag' => 'div',
                'attr' => ['class' => 'form-errors text-danger'],
                'value' => '',
                'label' => ''
            ])
            ->add('code', 'text', [
                'rules' => 'required',
                'label' => 'Ссылка на объект'
            ])
            ->add('name', 'text', [
            	'rules' => 'required',
            	'label' => 'Название'
            ])
            ->add('description', 'textarea', [
            	'rules' => 'required',
            	'label' => 'Описание'
            ])
            ->add('captcha', 'static', [
                'tag' => 'div',
                'label' => 'Капча',
                'attr' => ['class' => 'g-recaptcha', 'data-sitekey' => '6LezQSATAAAAAJYYGbUaNqNU646iYoqk4O75MtqV']
            ])
            ->add('save', 'submit', ['label' => 'Сохранить', 'attr' => ['class' => 'btn btn-success send-btn']])
        ;
    }
}
