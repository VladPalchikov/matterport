<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin');
    }

    public function update(Request $request, $id)
    {
        $item = \App\Item::find($id);
        $item->is_confirmed = true;
        $item->save();

        return redirect('/admin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Item::find($id)->delete();

        return redirect('/admin');
    }
}
