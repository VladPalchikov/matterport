<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Item;
use GuzzleHttp\Client;

use Kris\LaravelFormBuilder\FormBuilder;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(FormBuilder $formBuilder)
    {
        $form = $formBuilder->create(\App\Forms\ItemForm::class, [
            'method' => 'POST',
            'url' => '/'
        ]);

        $items = Item::orderBy('id')->get();
        return view('index', compact('items', 'form'));
    }

    public function show($id)
    {
        $item = Item::find($id);
        return view('item', compact('item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, FormBuilder $formBuilder)
    {
        $form = $formBuilder->create(\App\Forms\ItemForm::class);

        if (!$form->isValid()) {
            return json_encode(['status' => false, 'errors' => $form->getErrors()]);
        }

        $code = $request->get('code');

        $linkParts = parse_url($code);
        if (isset($linkParts['query'])) {
            parse_str($linkParts['query'], $query);
        } else {
            return json_encode(['status' => false, 'errors' => ['Не удалось обнаружить код в ссылке']]);
        }

        if (!empty($query['m'])) {
            $checkDublicate = Item::where('code', '=', $query['m'])->count();

            if ($checkDublicate > 0) {
                return json_encode(['status' => false, 'errors' => ['Такой объект уже добавлен']]);   
            }

            $client = new \GuzzleHttp\Client();
            $result = $client->request('GET', 'https://www.google.com/recaptcha/api/siteverify?secret=6LezQSATAAAAABJv2BKQ7zbK-_NNTIfCxfGzpcbT&response='.$request->get('g-recaptcha-response'));

            $captchaResult = json_decode($result->getBody());
            if ($captchaResult->success) {
                $item = new Item;
                $item->code = $query['m'];
                $item->name = $request->get('name');
                $item->description = $request->get('description');
                $item->save();
                return json_encode(['status' => true]);
            } else {
                return json_encode(['status' => false, 'errors' => ['Подтвердите, что вы не робот']]);
            }
        } else {
            return json_encode(['status' => false, 'errors' => ['Не удалось обнаружить код в ссылке']]);
        }
    }
}
