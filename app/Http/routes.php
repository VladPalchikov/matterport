<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(['middleware' => 'auth.basic'], function () {
	Route::resource('/admin', 'AdminController');
});

Route::get('/', 'IndexController@index');
Route::get('/{id}', 'IndexController@show');
Route::post('/save', 'IndexController@store');