<!DOCTYPE html>
<html lang="en">
    <head>
        <title></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <script src="https://code.jquery.com/jquery-1.12.3.min.js" integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ=" crossorigin="anonymous"></script>
        <!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <link rel="stylesheet" type="text/css" href="/css/main.css">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.3/normalize.min.css">
        <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>  
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <script src='/js/main.js'></script>
    </head>
    <body>
        <header>
            <div class="main-wrapper">
                <button type="button" class="btn add-btn" aria-label="Left Align" data-toggle="modal" data-target=".bs-example-modal-sm">
                    Добавить объект
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </button>
            </div>
        </header>
                
        <main>
            <div class="main-wrapper">
                <div class="row">
                	@foreach (\App\Item::orderBy('id', 'desc')->get() as $item)
						@if ($item->is_confirmed)
		                    <div class="col-sm-6">
		                        <div class="object-wrapper">
		                            <a href="/{{ $item->id }}">
		                                <img src="https://my.matterport.com/api/v1/player/models/{{ $item->code }}/thumb" alt="">            
		                                <div class="overlay"></div>
		                                <div class="description">
		                                    <span class="title">{{ $item->name }}</span>
		                                </div>
		                            </a>
		                        </div>
		                    </div>
	                    @endif
                    @endforeach
                </div>  
            </div>
        </main>
        <div class="modal fade bs-example-modal-sm object-form" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    {!! form($form) !!}
                </div>
            </div>
        </div>
    </body>
</html>