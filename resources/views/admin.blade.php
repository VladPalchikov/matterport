<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Admin</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Добавленные объекты</h1>
			</div>
		</div>

		<br>
		<br>

		<div class="row">
			<div class="col-md-12">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th class="col-md-1">Код</th>
							<th class="col-md-2">Название</th>
							<th class="col-md-8">Описание</th>
							<th class="col-md-1"></th>
						</tr>

						@foreach (\App\Item::orderBy('id')->get() as $item)
							<tr>
								<td>{{ $item->code }}</td>
								<td>{{ $item->name }}</td>
								<td>{{ $item->description }}</td>
								<td class="text-center">
									@if (!$item->is_confirmed)
									<form action="/admin/{{ $item->id }}" method="POST">
										<input name="_method" type="hidden" value="PUT">
										<input type="submit" class="btn btn-primary btn-sm" style="margin-bottom: 10px;" value="Подтвердить">
									</form>
									@endif
									<form action="/admin/{{ $item->id }}" method="POST">
										<input name="_method" type="hidden" value="DELETE">
										<input type="submit" class="btn btn-danger btn-sm" value="Удалить">
									</form>
								</td>
							</tr>
						@endforeach
					</thead>
				</table>
			</div>
		</div>
	</div>
</body>
</html>