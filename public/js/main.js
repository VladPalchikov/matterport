jQuery(document).ready(function($) {
	$('.object-form form').submit(function(event) {
		event.preventDefault();
		console.log(grecaptcha.getResponse());
		$.ajax({
			url: '/save',
			type: 'POST',
			data: {
				'code': $('input[name="code"]').val(),
				'name': $('input[name="name"]').val(),
				'description': $('textarea[name="description"]').val(),
				'g-recaptcha-response': grecaptcha.getResponse()
			},
		})
		.always(function(json) {
			var data = jQuery.parseJSON(json);
			console.log(data);
			if (!data.status) {
				$('.form-errors').text('');
				$.each(data.errors, function(index, val) {
					console.log(val);
					$('.form-errors').text($('.form-errors').text() + '\n' + val);
				});
			} else {
				window.location.replace("/");
			}
		});
		
	});
});